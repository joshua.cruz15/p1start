package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {
    @Override
    public int compare(Car o1, Car o2) {
        if(!o1.getCarBrand().equals(o2.getCarBrand())){
            return o1.getCarBrand().compareTo(o2.getCarBrand());
        } else if(!o1.getCarModel().equals(o2.getCarModel())){
            return o1.getCarModel().compareTo(o2.getCarModel());
        }else if(!o1.getCarModelOption().equals(o2.getCarModelOption())){
            return o1.getCarModelOption().compareTo(o2.getCarModelOption());
        }
        return 0;
    }
}
