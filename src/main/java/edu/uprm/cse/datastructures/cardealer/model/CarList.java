package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

public class CarList {
    private static final  SortedList<Car> cars = new CircularSortedDoublyLinkedList<>(new CarComparator());

    public static SortedList<Car> getInstance(){
        return cars;
    }

    public static void resetCars(){
        cars.clear();
    }
}
