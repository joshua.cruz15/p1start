package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {
    private Node<E> header;
    private int currentSize = 0;
    private Comparator<E> comparator;

    public CircularSortedDoublyLinkedList(Comparator<E> comparator){
        this.comparator = comparator;
        this.header = new Node<>();
        this.header.setNext(header);
    }

    private static class Node<E>{
        private E element;
        private Node<E> previous;
        private Node<E> next;
        Node(E element, Node<E> next){
            this.element = element;
            this.setNext(next);
        }
        Node(){
            super();
        }
        E getElement(){
            return this.element;
        }
        Node<E> getPrevious(){
            return this.previous;
        }
        Node<E> getNext() {
            return this.next;
        }

        void setElement(E element) {
            this.element = element;
        }

        void setPrevious(Node<E> previous) {
            this.previous = previous;
        }

        void setNext(Node<E> next) {
            this.next = next;
            next.setPrevious(this);
        }

        void clear(){
            this.element = null;
            this.next = null;
            this.previous = null;
        }
    }

    private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
        private Node<E> nextNode;

        CircularSortedDoublyLinkedListIterator(){
            this.nextNode = (Node<E>) header.getNext();
        }
        @Override
        public boolean hasNext() {
            return nextNode != header;
        }

        @Override
        public E next() {
            if(this.hasNext()){
                E result = this.nextNode.getElement();
                this.nextNode = this.nextNode.getNext();
                return result;
            }
            else{
                throw new NoSuchElementException();
            }
        }
    }

    @Override
    public boolean add(E obj) {
        Node<E> node = header;
        while(node.getNext()!=header && comparator.compare(node.getNext().getElement(), obj) < 0){
            node = node.getNext();
        }
        node.setNext(new Node<>(obj, node.getNext()));
        currentSize++;
        return true;
    }

    @Override
    public int size() {
        return currentSize;
    }

    @Override
    public boolean remove(E obj) {
        int index = firstIndex(obj);
        if (index >= 0) {
            return remove(index);
        }
        return false;
    }

    @Override
    public boolean remove(int index) {
        if ((index < 0) || (index >= currentSize)){
            throw new IndexOutOfBoundsException();
        }
        Node<E> node = getPosition(index);
        node.getPrevious().setNext(node.getNext());
        node.clear();
        currentSize--;
        return true;
    }

    @Override
    public int removeAll(E obj) {
        int counter = 0;
        while(firstIndex(obj) > 0){
            remove(firstIndex(obj));
            counter++;
        }
        return counter;
    }

    @Override
    public E first() {
        return header.getNext().getElement();
    }

    @Override
    public E last() {
        return header.getPrevious().getElement();
    }

    @Override
    public E get(int index) {
        if ((index < 0) || index >= currentSize) {
            throw new IndexOutOfBoundsException();
        }

        Node<E> temp  = this.getPosition(index);
        return temp.getElement();
    }

    public Node<E> getPosition(int index){
        int currentPosition = 0;
        Node<E> node = this.header.getNext();
        while(currentPosition != index){
            node = node.getNext();
            currentPosition++;
        }
        return node;
    }
    @Override
    public void clear() {
        while(header.getNext()!=header){
            remove(0);
        }
    }

    @Override
    public boolean contains(E e) {
        return firstIndex(e) > 0;
    }

    @Override
    public boolean isEmpty() {
        return currentSize == 0;
    }

    @Override
    public int firstIndex(E e) {
        int index = 0;
        for(Node<E> node = header.getNext(); node!=header; node=node.getNext(), index++){
            if(node.getElement().equals(e)){
                return index;
            }
        }
        return -1;
    }

    @Override
    public int lastIndex(E e) {
        int index = currentSize -1;
        for(Node<E> node = header.getPrevious(); node!=header; node=node.getPrevious(), index--){
            if(node.getElement().equals(e)){
                return index;
            }
        }
        return -1;
    }

    @Override
    public Iterator<E> iterator() {
        return new CircularSortedDoublyLinkedListIterator<E>();
    }
}
