package edu.uprm.cse.datastructures.cardealer;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.JsonError;
import edu.uprm.cse.datastructures.cardealer.util.NotFoundException;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/cars")
public class CarManager {
    /**
     * Auxiliary method for getting a car with a particular id
     * @param id
     * @return instance of Car with give id or null if car was not found
     */
    private Car getCarWithId(int id){
        for (Car car: CarList.getInstance()) {
            if (car.getCarId() == id) {
                return car;
            }
        }
        return null;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Car[] getAllCars() {
        Car[] result = new Car[CarList.getInstance().size()];
        int i = 0;
        for (Car car: CarList.getInstance()) {
            result[i] = car;
            i++;
        }
        return result;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Car getCar(@PathParam("id") int id ){
        Car car = getCarWithId(id);
        if (car != null){
            return car;
        } else {
            throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));
        }
    }

    @POST
    @Path("/add")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addCar(Car car){
        CarList.getInstance().add(car);
        return Response.status(201).build();
    }

    @PUT
    @Path("/{id}/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCar(@PathParam("id") int id, Car newCar){
        Car oldCar = getCarWithId(id);
        if(oldCar!=null){
            CarList.getInstance().remove(oldCar);
            CarList.getInstance().add(newCar);
            return Response.status(Response.Status.OK).build();
        } else{
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @DELETE
    @Path("/{id}/delete")
    public Response  deleteCar(@PathParam("id") int id){
        Car car = getCarWithId(id);
        if(car != null) {
            CarList.getInstance().remove(car);
            return Response.status(Response.Status.OK).build();
        } else{
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

}